package com.example.custom

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.confirm_dialog.*

class ConfirmDialog : DialogFragment() {
    var listenerConfirm : OnClickConfirmDetail?= null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog!!.window!!.setBackgroundDrawableResource(R.color.transparent)
        return inflater.inflate(R.layout.confirm_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_pick_image.setOnClickListener {
            listenerConfirm?.onClickPickImage()
        }

        btn_take_a_photo.setOnClickListener {
            listenerConfirm?.onClickTakeAPhoto()
        }

        btn_exit.setOnClickListener {
            listenerConfirm?.onClickExit()
        }
    }
    interface OnClickConfirmDetail{
        fun onClickPickImage()
        fun onClickTakeAPhoto()
        fun onClickExit()
    }
}