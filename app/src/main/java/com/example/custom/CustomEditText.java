package com.example.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.scientific_research.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class CustomEditText extends TextInputLayout {

    private TextInputEditText editText;
    public CustomEditText(@NonNull Context context) {
        super(context);
    }

    public CustomEditText(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, R.style.Widget_MaterialComponents_TextInputLayout_OutlinedBox);
    }

    public CustomEditText(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet atts , int def){
        setWillNotDraw(false);
        editText = new TextInputEditText(getContext());
        createBoxEdit(editText);
    }

    private void createBoxEdit(TextInputEditText editText){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        editText.setPadding(13, 13, 13, 13);
        editText.setLayoutParams(layoutParams);
        addView(editText);
    }
}
