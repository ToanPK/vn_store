package com.example.api

import com.google.gson.annotations.SerializedName

data class Response<T>(
    @SerializedName("status")
    val status : String = "",

    @SerializedName("code")
    val code : Int = -1,

    @SerializedName("data")
    val data : T?= null
)

fun Int.isSuccessResponse() : Boolean{
    return this == 200
}