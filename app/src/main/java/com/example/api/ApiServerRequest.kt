package com.example.api

import android.graphics.Bitmap
import com.example.model.AICheckAge
import com.example.model.AICheckComment
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import rx.Observable
import java.io.File
import java.util.*

interface ApiServerRequest {
    @Multipart
    @POST("getNoteText")
    fun postImage(
        @Part pic : MultipartBody.Part
    ) : Observable<AICheckAge>

    @POST("toxicdetection/{text}")
    fun postComment(
        @Path("text") text : String?
    ) : Call<AICheckComment>
}