package com.example.list

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.PopupMenu
import com.example.adapter.*
import com.example.base.BaseActivity
import com.example.base.initViewModel
import com.example.base.showToast
import com.example.constant.Constant
import com.example.database.AppDatabase
import com.example.detail.DetailActivity
import com.example.model.*
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.activity_list_product.*

class ListProductActivity : BaseActivity<ListProductViewModel>(), FashionAdapter.OnClickItemFashion,
    HousewareAdapter.OnClickItemHouseWare,
    ElectronicAdapter.OnClickElectronic,
    FastFoodAdapter.OnClickItemFood,
    CigaretteAdapter.OnClickCigarette{
    private lateinit var fashionAdapter : FashionAdapter
    private lateinit var housewareAdapter : HousewareAdapter
    private lateinit var eletronicAdapter : ElectronicAdapter
    private lateinit var fastFoodAdapter : FastFoodAdapter
    private lateinit var cigaretteAdapter : CigaretteAdapter
    override fun onCreateViewModel(): ListProductViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.activity_list_product

    override fun initView() {

        val keyFashion = intent?.extras?.getInt(Constant.KEY_FASHION)
        if (keyFashion == 1){
            tv_name_product_type.text = "Thời trang"
            fashionAdapter = FashionAdapter(AppDatabase.getDatabaseInstance(this).FashionDAO().getListFashion())
            fashionAdapter.listener = this
            rcv_list_product.adapter = fashionAdapter
        }

        val keyHouseWare = intent?.extras?.getInt(Constant.KEY_HOUSE_WARE)
        if (keyHouseWare == 2){
            tv_name_product_type.text = "Đồ gia dụng"
            housewareAdapter = HousewareAdapter(AppDatabase.getDatabaseInstance(this).houseWareDAO().getListHouseWare())
            housewareAdapter.listener = this
            rcv_list_product.adapter = housewareAdapter
        }

        val keyElectronic = intent?.extras?.getInt(Constant.KEY_ELECTRONIC)
        if (keyElectronic == 3){
            tv_name_product_type.text = "Thiết bị điện tử"
            eletronicAdapter = ElectronicAdapter(AppDatabase.getDatabaseInstance(this).electronicDAO().getListElectronic())
            eletronicAdapter.listener = this
            rcv_list_product.adapter = eletronicAdapter
        }

        val keyFastFood = intent?.extras?.getInt(Constant.KEY_FAST_FOOD)
        if (keyFastFood == 4){
            tv_name_product_type.text = "Đồ ăn"
            fastFoodAdapter = FastFoodAdapter(AppDatabase.getDatabaseInstance(this).fastFoodDAO().getListFastFood())
            fastFoodAdapter.listener = this
            rcv_list_product.adapter = fastFoodAdapter
        }

        val keyCigarette = intent?.extras?.getInt(Constant.KEY_CIGARETTE)
        if (keyCigarette == 5){
            tv_name_product_type.text = "Thuốc lá"
            cigaretteAdapter = CigaretteAdapter(AppDatabase.getDatabaseInstance(this).cigaretteDAO().getListCigarette())
            cigaretteAdapter.listener = this
            rcv_list_product.adapter = cigaretteAdapter
        }
    }

    override fun observerData() {

    }
    override fun onClickFashionDetail(position: Int, fashion: Fashion) {
        val bundle = Bundle()
        bundle.putSerializable(Constant.FASHION,fashion)
        val intent = Intent(this, DetailActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
    }

    override fun onClickFashionAddCart(position: Int, seeMore : ImageView, fashion: Fashion) {
        val popupMenu = PopupMenu(this, seeMore)
        popupMenu.inflate(R.menu.item_pop_up)

        popupMenu.setOnMenuItemClickListener { menuItem ->
            if (menuItem.itemId == R.id.add_product){
                AppDatabase.getDatabaseInstance(this).cartDAO().insertCart(
                    Cart(null, fashion, null, null, null, null)
                )

                Log.d("data", AppDatabase.getDatabaseInstance(this).cartDAO().getListCart().size.toString())
                 showToast("Đã thêm thành công sản phẩm vào giỏ!", this)
            }
            true
        }

        popupMenu.show()
    }

    override fun onClickHouseWare(position: Int, seeMore: ImageView, houseware: Houseware) {

    }

    override fun onClick(houseware: Houseware) {

    }

    override fun onClick(cigarette: Cigarette) {

    }

    override fun onClick(electronicDevice: ElectronicDevice) {

    }

    override fun onClick(food: FastFood) {
    }
}