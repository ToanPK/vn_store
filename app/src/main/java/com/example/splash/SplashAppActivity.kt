package com.example.splash

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.example.base.BaseActivity
import com.example.base.initViewModel
import com.example.scientific_research.MainActivity
import com.example.scientific_research.R
import java.util.logging.Handler

class SplashAppActivity : BaseActivity<SplashAppViewModel>() {
    private var TIME_NEXT = 2000L
    override fun onCreateViewModel(): SplashAppViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.splash_app_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("onCreate", "1")
    }

    override fun onStart() {
        super.onStart()
        Log.d("onStart", "2")
    }

    override fun onResume() {
        super.onResume()
        Log.d("onResume", "3")
    }

    override fun onPause() {
        super.onPause()
        Log.d("onPause", "4")
    }

    override fun onStop() {
        super.onStop()
        Log.d("onStop", "5")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("onRestart", "6")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("onDestroy", "7")
    }

    override fun initView() {
         android.os.Handler().postDelayed({
             startActivity(Intent(this, MainActivity::class.java).apply {
                 setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
             })
         }, TIME_NEXT)


    }

    override fun observerData() {

    }


}