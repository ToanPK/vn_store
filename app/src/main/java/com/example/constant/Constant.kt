package com.example.constant

object Constant {
    const val DELAY_MS = 500L
    const val PERIOD_MS = 3000L
    const val PRODUCT = "product"
    const val PRODUCT_CART = "Cart"
    const val FASHION = "fashion"
    const val PRODUCT_CIGARETTE = "cigarette"
    const val PRODUCT_ELECTRONIC = "electronic"
    const val PRODUCT_FASHION = "fashion"
    const val PRODUCT_HOUSE_WARE = "houseWare"
    const val PRODUCT_FAST_FOOD = "fastFood"
    const val INDEX_FASHION = "index_fashion"
    const val BASE_URL = "http://192.168.1.33:5005/"
    const val PICK_IMAGE_REQUEST = 1888
    const val CAMERA_REQUEST = 1009
    const val MY_CAMERA_PERMISSION_CODE = 100
    const val PIC_PARAM = "pic"
    const val READ_EXTERNAL_REQUEST = 2
    const val DURATION_3000L = 3000L
    const val KEY_FASHION = "1"
    const val KEY_HOUSE_WARE = "2"
    const val KEY_ELECTRONIC = "3"
    const val KEY_FAST_FOOD = "4"
    const val KEY_CIGARETTE = "5"
    const val IMAGE_REQUEST = 12
}