package com.example.constant

import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.content.CursorLoader
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


object RealPathUtil {
    fun getPathFromURI(context: Context, contentUri: Uri?): String? {
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor? =
            contentUri?.let { context.contentResolver.query(it, proj, null, null, null) }
        if (cursor!!.moveToFirst()) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(column_index)
        }
        cursor.close()
        return res
    }

    fun convertBitmapIntoFile(context: Context, photo: Bitmap): File {
        var fileBitmap: File? = null
        val timeStampp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val timeStampp2: String = SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(Date())
        val imageFileName = "IMG_" + timeStampp + "_"
        val storageDir: File? =
            context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val file = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )
        context.filesDir
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            photo.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.flush()
            out.close()
            val arr = timeStampp2.split("_").toTypedArray()
            val file_path = file.getAbsolutePath()
            fileBitmap = File(file_path)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return fileBitmap!!
    }
}