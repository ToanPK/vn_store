package com.example.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AICheckAge(
    @SerializedName("age")
    val age : String?
) : Serializable{
}