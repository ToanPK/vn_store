package com.example.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AICheckComment(
    @SerializedName("toxic")
    val toxic : String?
) : Serializable