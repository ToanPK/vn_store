package com.example.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@IgnoreExtraProperties
@Entity
class FastFood(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    val id : Int,

    @SerializedName("name")
    val name : String,

    @SerializedName("price")
    val price : Long,

    @SerializedName("image")
    val image : String,

    @SerializedName("image1")
    val image1 : String,

    @SerializedName("isCheck")
    val isCheck : Boolean,

    @SerializedName("number")
    var number : Int,

    @SerializedName("description")
    val description : String,

    @SerializedName("numberProduct")
    val numberProduct : Long,
) : Serializable{
   @Exclude
   fun toMap() : Map<String, Any?>{
       return mapOf(
           "id" to id,
           "name" to name,
           "price" to price,
           "image" to image,
           "image1" to image1,
           "isCheck" to isCheck,
           "number" to number,
           "description" to description,
           "numberProduct" to numberProduct
       )
   }
}