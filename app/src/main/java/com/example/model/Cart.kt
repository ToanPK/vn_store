package com.example.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity
class Cart(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    val id: Int?,

    @SerializedName("fashion")
    val fashion: Fashion?,

    @SerializedName("electronic")
    val electronic: ElectronicDevice?,

    @SerializedName("houseware")
    val houseware: Houseware?,

    @SerializedName("fastFood")
    val fastFood: FastFood?,

    @SerializedName("Cigarette")
    val cigarette: Cigarette?,
) {
}