package com.example.model

import com.google.gson.annotations.SerializedName

class User(
    @SerializedName("id")
    val id : String?="",

    @SerializedName("userName")
    val userName : String?="",

    @SerializedName("imageUrl")
    val imageUrl : String?=""
) {
}