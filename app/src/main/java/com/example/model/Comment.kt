package com.example.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Comment(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("uid")
    val uid: Int?=null,

    @SerializedName("description")
    val description: String? = "",

    @SerializedName("dateTime")
    val dateTime: String? = "",

    @SerializedName("name")
    val name : String?= ""
)

