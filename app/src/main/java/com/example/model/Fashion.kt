package com.example.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
class Fashion(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    val id : Int?,

    @SerializedName("name")
    val name : String?,

    @SerializedName("price")
    val price : Long?,

    @SerializedName("image")
    val image : String?,

    @SerializedName("isCheck")
    val isCheck : Boolean?,

    @SerializedName("number")
    val number : Int?,

    @SerializedName("description")
    val description : String?,

    @SerializedName("numberProduct")
    val numberProduct : Long?,

    @SerializedName("listComment")
    val listComment : List<Comment>?

) : Serializable{

}
