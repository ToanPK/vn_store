package com.example.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity
class Product (
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    val id : Int,

    @SerializedName("name")
    val name : String,

    @SerializedName("price")
    val price : Long,

    @SerializedName("image")
    val image : String,

    @SerializedName("image1")
    val image1 : String?,

    @SerializedName("isCheck")
    val isCheck : Boolean,

    @SerializedName("number")
    val number : Int,

    @SerializedName("description")
    val description : String,

    @SerializedName("numberProduct")
    val numberProduct : Long,
)