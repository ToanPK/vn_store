package com.example.scientific_research

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.base.BaseActivity
import com.example.base.initViewModel
import com.example.base.isValidateEmail
import com.example.base.showToast
import com.example.home.HomeActivity
import com.example.signup.SignUpActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class MainActivity : BaseActivity<MainViewModel>() {

    private val authen = FirebaseAuth.getInstance()
    override fun onCreateViewModel(): MainViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.activity_main

    override fun initView() {
        btn_next_sign_up.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }

        btn_sign_in.setOnClickListener {
            signIn()
        }
    }

    override fun observerData() {

    }

    private fun signIn(){
        if (edt_phone_number.text.toString().trim().isEmpty()){
            showToast("Vui lòng nhập email!",this)
        }else{
            if (!isValidateEmail(edt_phone_number.text.toString().trim())){
                showToast("Định dạng email không hợp lệ!", this)
            }else{
                if (edt_pass.text.toString().trim().isEmpty()){
                    showToast("Vui lòng nhập mật khẩu!", this)
                }else{
                    showOrHideProgressDialog(true)
                    authen.signInWithEmailAndPassword(
                        edt_phone_number.text.toString().trim(),
                        edt_pass.text.toString().trim()
                    ).addOnCompleteListener { task ->
                        showOrHideProgressDialog(false)
                        showToast("Đăng nhập thành công!", this)

                        if (!task.isSuccessful){
                            showToast("Đăng nhập thất bại!", this)
                        }else{
                            val intent = Intent(this, HomeActivity::class.java).apply {
                                flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            }
                            startActivity(intent)
                        }
                    }
                }
            }

        }
    }
}