package com.example.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.example.base.loadImage
import com.example.scientific_research.R

class DetailProductAdapter(private val mListImageDetail : List<String>, private val context: Context) : PagerAdapter() {
    override fun getCount(): Int {
        return mListImageDetail.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val viewItem = inflater.inflate(R.layout.item_banner_detail, container, false)
            val image_banner = viewItem.findViewById<ImageView>(R.id.img_banner)
            image_banner.loadImage(mListImageDetail[position])
            container.addView(viewItem)

            return viewItem
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }
}