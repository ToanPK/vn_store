package com.example.adapter

import android.app.Activity
import android.content.Context
import android.media.Image
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.contentValuesOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.scientific_research.R

class HomeAdapter(private val listImage : List<Int>, private val context: Context) : PagerAdapter(){
    override fun getCount(): Int = listImage.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return  view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val viewItem = inflater.inflate(R.layout.item_banner, container, false)
        val image_banner = viewItem.findViewById<ImageView>(R.id.img_banner)
        image_banner.setImageResource(listImage[position])
        container.addView(viewItem)

        return viewItem
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
         container.removeView(`object` as View)
    }

}