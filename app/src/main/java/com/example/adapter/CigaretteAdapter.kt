package com.example.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.base.asMoney
import com.example.base.loadImage
import com.example.model.Cigarette
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.item_fashion.view.*

class CigaretteAdapter(private val mListCigarette : List<Cigarette>) : RecyclerView.Adapter<CigaretteViewHolder>() {
    var listener : OnClickCigarette?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CigaretteViewHolder {
        return CigaretteViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_fashion,parent, false)
        )
    }

    override fun onBindViewHolder(holder: CigaretteViewHolder, position: Int) {
        holder.itemView.run {
            tv_name_fashion.text = mListCigarette[position].name
            tv_price_fashion.text = mListCigarette[position].price.asMoney()
            img_fashion.loadImage(mListCigarette[position].image)
            setOnClickListener {
                listener?.onClick(mListCigarette[position])
            }
        }
    }

    override fun getItemCount(): Int{
        return mListCigarette.size
    }

    interface OnClickCigarette{
        fun onClick(cigarette: Cigarette)
    }


}

class CigaretteViewHolder(item : View) : RecyclerView.ViewHolder(item){

}