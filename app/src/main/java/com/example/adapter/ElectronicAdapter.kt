package com.example.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.base.asMoney
import com.example.base.loadImage
import com.example.model.ElectronicDevice
import com.example.model.FastFood
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.item_fashion.view.*

class ElectronicAdapter(val mListElectronic : List<ElectronicDevice>) : RecyclerView.Adapter<ElectronicViewHolder>(){
    var listener : OnClickElectronic?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ElectronicViewHolder {
        return ElectronicViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_fashion, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ElectronicViewHolder, position: Int) {
        holder.binData(mListElectronic[position], listener)
    }

    override fun getItemCount(): Int = mListElectronic.size
    interface OnClickElectronic{
        fun onClick(electronicDevice: ElectronicDevice)
    }
}

class ElectronicViewHolder(item : View) : RecyclerView.ViewHolder(item){
      fun binData(electronicDevice: ElectronicDevice, listener : ElectronicAdapter.OnClickElectronic?){
          itemView.run {
              setOnClickListener {
                  listener?.onClick(electronicDevice)
              }
              tv_name_fashion.text = electronicDevice.name
              img_fashion.loadImage(electronicDevice.image)
              tv_price_fashion.text = electronicDevice.price.asMoney()
          }
      }
}