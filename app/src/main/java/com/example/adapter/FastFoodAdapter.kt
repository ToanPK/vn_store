package com.example.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.base.asMoney
import com.example.base.loadImage
import com.example.model.FastFood
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.item_fashion.view.*

class FastFoodAdapter(val mListFastFood : List<FastFood>) : RecyclerView.Adapter<FastFoodViewHolder>() {
    var listener : OnClickItemFood?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FastFoodViewHolder {
        return FastFoodViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_fashion, parent, false)
        )
    }

    override fun onBindViewHolder(holder: FastFoodViewHolder, position: Int) {
        holder.binData(mListFastFood[position], listener)
    }

    override fun getItemCount(): Int =mListFastFood.size
    interface OnClickItemFood{
        fun onClick(food: FastFood)
    }

}

class FastFoodViewHolder(item : View) : RecyclerView.ViewHolder(item){
    fun binData(fastFood : FastFood, listener : FastFoodAdapter.OnClickItemFood?){
        itemView.run {
            setOnClickListener {
                listener?.onClick(fastFood)
            }
            tv_price_fashion.text = fastFood.price.asMoney()
            tv_name_fashion.text = fastFood.name
            img_fashion.loadImage(fastFood.image)
        }
    }
}