package com.example.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.base.formatDate
import com.example.model.Comment
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentAdapter : ListAdapter<Comment, CommentViewHolder>(DiffCallBack()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bindData(getItem(position))
    }
}

class CommentViewHolder(item : View) : RecyclerView.ViewHolder(item){
      fun bindData(comment: Comment){
          itemView.run {
              tv_description_comment.text = comment.description
              tv_date_time.text = comment.dateTime
          }
      }
}

class DiffCallBack : DiffUtil.ItemCallback<Comment>(){
    override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem == newItem
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem == newItem
    }

}