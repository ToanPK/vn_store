package com.example.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.example.base.asMoney
import com.example.base.loadImage
import com.example.model.Fashion
import com.example.model.Houseware
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.item_fashion.view.*

class HousewareAdapter(val listHouseware : List<Houseware>) : RecyclerView.Adapter<HousewareViewHolder>() {
    var listener : OnClickItemHouseWare?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HousewareViewHolder {
        return HousewareViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_fashion, parent, false)
        )
    }

    override fun onBindViewHolder(holder: HousewareViewHolder, position: Int) {
        listener?.let { holder.binData(listHouseware[position], it) }
    }

    override fun getItemCount(): Int {
        return listHouseware.size
    }

    interface OnClickItemHouseWare{
        fun onClickHouseWare(position: Int, seeMore : ImageView, houseware: Houseware)
        fun onClick(houseware: Houseware)
    }


}

class HousewareViewHolder(item : View) : RecyclerView.ViewHolder(item){
    fun binData(houseware: Houseware, listener : HousewareAdapter.OnClickItemHouseWare){
        itemView.run {
            img_fashion.loadImage(houseware.image)
            tv_name_fashion.text = houseware.name
            tv_price_fashion.text = houseware.price?.asMoney()
            btn_see_more.setOnClickListener {
                listener.onClickHouseWare(adapterPosition, btn_see_more, houseware)
            }
            setOnClickListener {
                listener.onClick(houseware)
            }
        }
    }
}