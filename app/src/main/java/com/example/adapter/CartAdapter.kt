package com.example.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.base.DiffUtilItemCallBack
import com.example.base.asMoney
import com.example.base.loadImage
import com.example.model.Cart
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.item_cart.view.*

class CartAdapter : ListAdapter<Cart, CartViewHolder>(DiffUtilItemCallBack<Cart>()) {
    var listener : OnClickProductCart?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        return CartViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
            )

    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.binDataCart(getItem(position), listener)
    }

    interface OnClickProductCart{
        fun onClick(cart: Cart)
    }

}



class CartViewHolder(item : View) : RecyclerView.ViewHolder(item){
      fun binDataCart(cart: Cart, listener : CartAdapter.OnClickProductCart?){
         itemView.run {
             btn_delete.setOnClickListener {
                 listener?.onClick(cart)
             }
             if (cart.fashion != null){
                 tv_name_cart_product.text = cart.fashion.name
                 tv_price_cart_product.text = cart.fashion.price?.asMoney()
                 img_cart_product.loadImage(cart.fashion.image)
             }
             if (cart.houseware != null){
                 tv_name_cart_product.text = cart.houseware.name
                 tv_price_cart_product.text = cart.houseware.price?.asMoney()
                 img_cart_product.loadImage(cart.houseware.image)
             }

             if (cart.electronic != null){
                 tv_name_cart_product.text = cart.electronic.name
                 tv_price_cart_product.text = cart.electronic.price.asMoney()
                 img_cart_product.loadImage(cart.electronic.image)
             }

             if (cart.fastFood != null){
                 tv_name_cart_product.text = cart.fastFood.name
                 tv_price_cart_product.text = cart.fastFood.price.asMoney()
                 img_cart_product.loadImage(cart.fastFood.image)
             }

             if (cart.cigarette != null){
                 tv_name_cart_product.text = cart.cigarette.name
                 tv_price_cart_product.text = cart.cigarette.price.asMoney()
                 img_cart_product.loadImage(cart.cigarette.image)
             }

         }

     }
}

