package com.example.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.example.base.asMoney
import com.example.base.loadImage
import com.example.model.Fashion
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.item_fashion.view.*

class FashionAdapter(val listFashion : List<Fashion>) : RecyclerView.Adapter<FashionViewHolder>() {
    var listener : OnClickItemFashion?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FashionViewHolder {
        return FashionViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_fashion, parent, false)
        )
    }

    override fun onBindViewHolder(holder: FashionViewHolder, position: Int) {
        holder.binData(listFashion[position], listener)
    }

    override fun getItemCount(): Int {
        return listFashion.size
    }

    interface OnClickItemFashion{
        fun onClickFashionDetail(position: Int, fashion: Fashion)
        fun onClickFashionAddCart(position: Int, seeMore : ImageView, fashion: Fashion)
    }


}

class FashionViewHolder(item : View) : RecyclerView.ViewHolder(item){
    fun binData(fashion : Fashion, listener : FashionAdapter.OnClickItemFashion?){
        itemView.run {
            img_fashion.loadImage(fashion.image)
            tv_name_fashion.text = fashion.name
            tv_price_fashion.text = fashion.price?.asMoney()
            setOnClickListener {
                listener?.onClickFashionDetail(adapterPosition, fashion)
            }
            btn_see_more.setOnClickListener {
                listener?.onClickFashionAddCart(adapterPosition, btn_see_more, fashion)
            }

        }
    }
}