package com.example.signup

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import com.example.base.*
import com.example.home.HomeActivity
import com.example.scientific_research.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : BaseActivity<SignUpViewModel>() {
    private lateinit var authen: FirebaseAuth
    private lateinit var reference: DatabaseReference

    override fun onCreateViewModel(): SignUpViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.activity_sign_up

    override fun initView() {
        btn_sign_up.setOnClickListener {
            signUp()
        }
        btn_back.setOnClickListener { onBackPressed() }

    }

    override fun observerData() {

    }

    private fun signUp() {
        if (edt_email_sign_up.text.toString().trim().isEmpty()) {
            showToast("vui lòng nhập email!", this)
        } else {
            if (!isValidateEmail(edt_email_sign_up.text.toString().trim())) {
                showToast("Định dạng email không hợp lệ!", this)
            } else {
                if (edt_pass_sign_up.text.toString().trim().isEmpty()) {
                    showToast("vui lòng nhập mật khẩu!", this)
                } else {
                    if (edt_pass_sign_up.text.toString().trim().length < 6) {
                        showToast("Mât khẩu không an toàn!", this)
                    } else {
                        if (edt_pass_confirm.text.toString().trim().isEmpty()) {
                            showToast("vui lòng xác nhận mật khẩu!", this)
                        } else {
                            if (edt_pass_sign_up.text.toString()
                                    .trim() != edt_pass_confirm.text.toString().trim()
                            ) {
                                showOrHideProgressDialog(false)
                                showToast("Mật khẩu không khớp!", this)
                            } else {
                                if (edt_full_name.text.toString().trim().isEmpty()) {
                                    showToast("Vui lòng nhập họ tên!", this)
                                } else {
                                    showOrHideProgressDialog(true)
                                    authen = FirebaseAuth.getInstance()
                                    authen.createUserWithEmailAndPassword(
                                        edt_email_sign_up.text.toString().trim(),
                                        edt_pass_sign_up.text.toString().trim()
                                    ).addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            val user: FirebaseUser = authen.currentUser!!
                                            val userID = user.uid
                                            reference =
                                                FirebaseDatabase.getInstance().getReference("Users")
                                                    .child(userID)
                                            val hashMap: HashMap<String, Any?> = hashMapOf()
                                            hashMap["id"] = userID
                                            hashMap["userName"] = edt_full_name.text.toString()
                                            hashMap["imageUrl"] = "default"
                                            reference.setValue(hashMap)
                                                .addOnCompleteListener { task ->
                                                    if (task.isSuccessful) {
                                                        val intent = Intent(
                                                            this,
                                                            HomeActivity::class.java
                                                        ).apply {
                                                            flags =
                                                                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                                                        }
                                                        startActivity(intent)
                                                        finish()
                                                    } else {
                                                        Toast(this).showCustomToast(
                                                            "Đăng ký thất bại!",
                                                            false,
                                                            this
                                                        )
                                                    }
                                                }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }

            }
        }


    }
}