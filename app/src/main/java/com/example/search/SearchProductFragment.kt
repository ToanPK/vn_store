package com.example.search

import android.app.SearchManager
import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.getSystemService
import com.example.base.BaseFragment
import com.example.base.initViewModel
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.fragment_search_product.*


class SearchProductFragment : BaseFragment<SearchProductViewModel>(){
    companion object{
        fun newInstance() = SearchProductFragment()
    }
    override fun layoutId(): Int = R.layout.fragment_search_product

    override fun initView() {
        val searchManager = context?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        search.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
    }

    override fun observerData() {

    }

    override fun onCreateViewModel(): SearchProductViewModel = initViewModel()

}

