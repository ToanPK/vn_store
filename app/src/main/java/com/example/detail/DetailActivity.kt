package com.example.detail

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.R.attr.bitmap
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.text.format.DateFormat
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.adapter.CommentAdapter
import com.example.adapter.DetailProductAdapter
import com.example.api.ApiService
import com.example.base.*
import com.example.constant.Constant
import com.example.constant.RealPathUtil
import com.example.custom.ConfirmDialog
import com.example.database.AppDatabase
import com.example.model.*
import com.example.scientific_research.R
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DetailActivity : BaseActivity<DetailViewModel>(), ConfirmDialog.OnClickConfirmDetail {
    private val listImageDetail = ArrayList<String>()
    private lateinit var detailApdater: DetailProductAdapter
    private val dialogConfirm = ConfirmDialog()
    private var currentPager = 0
    private val mListComment = ArrayList<Comment>()
    private val mCommentAdapter = CommentAdapter()
    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private val reference = FirebaseDatabase.getInstance().reference
    override fun onCreateViewModel(): DetailViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.activity_detail

    override fun initView() {
        dialogConfirm.listenerConfirm = this
        btn_add_cart.setOnClickListener {

        }

    }

    @SuppressLint("SetTextI18n")
    override fun observerData() {
        getDataFashionFromHome()
        getDataHousewareFromHome()
        getDataCigaretteFromHome()
        getDataFastFoodFromHome()
        getDataElectronicFromHome()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constant.CAMERA_REQUEST -> {
                if (resultCode == RESULT_OK) {
                    val photo = data?.extras?.get("data") as Bitmap
                    getResponseFromServerByCamera(photo)
                }
            }

            Constant.PICK_IMAGE_REQUEST -> {
                if (resultCode == RESULT_OK && data != null && data.data != null) {
                    val uri: Uri = data.data as Uri
                    getResponseFromServer(uri)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constant.MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, Constant.CAMERA_REQUEST)
            } else {
                Log.e("permission denied", "camera permission denied")
            }
        }
    }

    private fun getResponseFromServer(uri: Uri) {
        viewModel.apply {
            isLoading.observe(this@DetailActivity) {
                showOrHideProgressDialog(it)
                dialogConfirm.dismiss()
            }
            uploadFileToServer(uri, this@DetailActivity)
            responseCheckAge.observe(this@DetailActivity) { age ->
                if (age.substring(age.indexOf("(") + 1, age.indexOf("-")).toInt() < 8) {
                    Toast(this@DetailActivity).showCustomToast(
                        "Bạn chưa đủ tuổi để mua mặt hàng này!",
                        false,
                        this@DetailActivity
                    )
                    dialogConfirm.dismiss()
                } else {
                    Toast(this@DetailActivity).showCustomToast(
                        "Bạn đã đủ điều kiện để mua mặt hàng này",
                        true,
                        this@DetailActivity
                    )
                    dialogConfirm.dismiss()
                }
            }
        }
    }

    private fun getResponseFromServerByCamera(photo: Bitmap) {
        RealPathUtil.convertBitmapIntoFile(this, photo)
        viewModel.apply {
            isLoading.observe(this@DetailActivity) {
                showOrHideProgressDialog(it)
            }
            uploadFileToServerByCamera(
                RealPathUtil.convertBitmapIntoFile(
                    this@DetailActivity,
                    photo
                ), this@DetailActivity
            )
            responseCheckAge.observe(this@DetailActivity) { age ->
                if (age.substring(age.indexOf("(") + 1, age.indexOf("-")).toInt() < 18) {
                    Toast(this@DetailActivity).showCustomToast(
                        "Bạn chưa đủ tuổi để mua mặt hàng này!",
                        false,
                        this@DetailActivity
                    )
                    dialogConfirm.dismiss()
                } else {
                    Toast(this@DetailActivity).showCustomToast(
                        "Bạn đã đủ điều kiện để mua mặt hàng này",
                        true,
                        this@DetailActivity
                    )
                    dialogConfirm.dismiss()
                }
            }
        }
    }

    private fun loadImageDetail() {
        if (listImageDetail.size > 0) {
            detailApdater = DetailProductAdapter(listImageDetail, this)
            vp_detail.adapter = detailApdater
            indicator_detail.setViewPager(vp_detail)

            val update = Runnable {
                if (currentPager == listImageDetail.size) {
                    currentPager = 0
                }
                try {
                    vp_detail.setCurrentItem(currentPager++, true)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

            val timer = Timer()
            val handler = Handler()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    handler.post(update)
                }
            }, Constant.DELAY_MS, Constant.PERIOD_MS)
        }

    }

    private fun requestPermissionAndPickImage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            pickImage()
            return
        }
        val result = ContextCompat.checkSelfPermission(
            this,
            READ_EXTERNAL_STORAGE
        )
        if (result == PackageManager.PERMISSION_GRANTED) {
            pickImage()
        } else {
            requestPermissions(
                arrayOf(
                    READ_EXTERNAL_STORAGE
                ), Constant.READ_EXTERNAL_REQUEST
            )
        }
    }

    private fun pickImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_PICK
        startActivityForResult(
            Intent.createChooser(intent, "Select a File to Upload"),
            Constant.PICK_IMAGE_REQUEST
        )
    }

    private fun showCamera() {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA),
                Constant.MY_CAMERA_PERMISSION_CODE
            )
        } else {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, Constant.CAMERA_REQUEST)
        }
    }

    private fun formatDate(): String {
        val calendar = Calendar.getInstance(Locale.ENGLISH).time
        return DateFormat.format("hh:mm (dd/MM/yyyy)", calendar).toString()
    }


    override fun onClickPickImage() {
        requestPermissionAndPickImage()
    }

    override fun onClickTakeAPhoto() {
        showCamera()
    }

    override fun onClickExit() {
        dialogConfirm.dismiss()
    }

    private fun getDataFashionFromHome() {
        val fashion = intent?.extras?.getSerializable(Constant.FASHION) as? Fashion
        if (fashion != null) {
            btn_buy_now.setOnClickListener {
                Toast(this).showCustomToast("Mua thành công", true, this)
            }
            tv_name_product_detail.text = fashion.name
            tv_price_product_detail.text = fashion.price?.asMoney()
            tv_description_product_detail.text = fashion.description
            fashion.image?.let { listImageDetail.add(it) }
            loadImageDetail()
            btn_send_comment.setOnClickListener {
                showOrHideProgressDialog(true)
                if (edt_comment.text.toString() == "") {
                    Toast(this).showCustomToast("không để bình luận trống!", false, this)
                } else {
                    ApiService.apiRequest().postComment(edt_comment.text.toString())
                        .enqueue(object : Callback<AICheckComment> {
                            override fun onResponse(
                                call: Call<AICheckComment>,
                                response: retrofit2.Response<AICheckComment>
                            ) {
                                if (response.isSuccessful) {
                                    showOrHideProgressDialog(false)
                                    if (response.body()!!.toxic == "0") {
                                        AppDatabase.getDatabaseInstance(this@DetailActivity)
                                            .commentDAO().insertComment(
                                            Comment(
                                                null,
                                                edt_comment.text.toString(),
                                                formatDate(),
                                                fashion.name.toString()
                                            )
                                        )
                                        mCommentAdapter.submitList(
                                            AppDatabase.getDatabaseInstance(this@DetailActivity)
                                                .commentDAO().getListComment(fashion.name.toString())
                                        )
                                        rcv_cmt.adapter = mCommentAdapter
                                        tv_comment_number.text =
                                            "(" + AppDatabase.getDatabaseInstance(this@DetailActivity)
                                                .commentDAO().getListComment(fashion.name.toString()).size.toString() + ")"

                                    } else {
                                        Toast(this@DetailActivity).showCustomToast(
                                            "Từ ngữ bình luận không phù hợp",
                                            false,
                                            this@DetailActivity
                                        )
                                    }
                                }
                            }

                            override fun onFailure(call: Call<AICheckComment>, t: Throwable) {
                                showOrHideProgressDialog(false)
                                Log.e("error_sv", t.message.toString())
                            }

                        })
                }
            }
            mCommentAdapter.submitList(
                AppDatabase.getDatabaseInstance(this)
                    .commentDAO().getListComment(fashion.name.toString())
            )
            rcv_cmt.adapter = mCommentAdapter
            tv_comment_number.text = "(" + AppDatabase.getDatabaseInstance(this)
                .commentDAO().getListComment(fashion.name.toString())
                .size.toString() + ")"
        }
    }

    private fun getDataHousewareFromHome() {
        val houseWare = intent?.extras?.getSerializable(Constant.PRODUCT_HOUSE_WARE) as? Houseware
        if (houseWare != null) {
            tv_name_product_detail.text = houseWare.name
            tv_price_product_detail.text = houseWare.price?.asMoney()
            tv_description_product_detail.text = houseWare.description
            houseWare.image?.let { listImageDetail.add(it) }
            loadImageDetail()
            btn_send_comment.setOnClickListener {
                showOrHideProgressDialog(true)
                if (edt_comment.text.toString() == "") {
                    Toast(this).showCustomToast("không để bình luận trống!", false, this)
                } else {
                    ApiService.apiRequest().postComment(edt_comment.text.toString())
                        .enqueue(object : Callback<AICheckComment> {
                            override fun onResponse(
                                call: Call<AICheckComment>,
                                response: retrofit2.Response<AICheckComment>
                            ) {
                                if (response.isSuccessful) {
                                    showOrHideProgressDialog(false)
                                    if (response.body()!!.toxic == "0") {
                                        AppDatabase.getDatabaseInstance(this@DetailActivity)
                                            .commentDAO().insertComment(
                                                Comment(
                                                    null,
                                                    edt_comment.text.toString(),
                                                    formatDate(),
                                                    houseWare.name.toString()
                                                )
                                            )
                                        mCommentAdapter.submitList(
                                            AppDatabase.getDatabaseInstance(this@DetailActivity)
                                                .commentDAO().getListComment(houseWare.name.toString())
                                        )
                                        rcv_cmt.adapter = mCommentAdapter
                                        tv_comment_number.text =
                                            "(" + AppDatabase.getDatabaseInstance(this@DetailActivity)
                                                .commentDAO().getListComment(houseWare.name.toString()).size.toString() + ")"

                                    } else {
                                        Toast(this@DetailActivity).showCustomToast(
                                            "Từ ngữ bình luận không phù hợp",
                                            false,
                                            this@DetailActivity
                                        )
                                    }
                                }
                            }

                            override fun onFailure(call: Call<AICheckComment>, t: Throwable) {
                                showOrHideProgressDialog(false)
                                Log.e("error_sv", t.message.toString())
                            }

                        })
                }
            }
            mCommentAdapter.submitList(
                AppDatabase.getDatabaseInstance(this)
                    .commentDAO().getListComment(houseWare.name.toString())
            )
            rcv_cmt.adapter = mCommentAdapter
            tv_comment_number.text = "(" + AppDatabase.getDatabaseInstance(this)
                .commentDAO().getListComment(houseWare.name.toString())
                .size.toString() + ")"
        }

    }

    private fun getDataCigaretteFromHome(){
        val cigarette = intent?.extras?.getSerializable(Constant.PRODUCT_CIGARETTE) as? Cigarette?
        if (cigarette != null){
            btn_buy_now.setOnClickListener {
                dialogConfirm.show(supportFragmentManager, "")
                dialogConfirm.isCancelable = false
            }
            tv_name_product_detail.text = cigarette.name
            tv_price_product_detail.text = cigarette.price.asMoney()
            tv_description_product_detail.text = cigarette.description
            cigarette.image.let { listImageDetail.add(it) }
            listImageDetail.add(cigarette.image1)
            loadImageDetail()
        }
    }

    private fun getDataElectronicFromHome(){
        val electronic = intent?.extras?.getSerializable(Constant.PRODUCT_ELECTRONIC) as? ElectronicDevice?
        if (electronic != null){
            btn_buy_now.setOnClickListener {
                dialogConfirm.show(supportFragmentManager, "")
                dialogConfirm.isCancelable = false
            }
            tv_name_product_detail.text = electronic.name
            tv_price_product_detail.text = electronic.price.asMoney()
            tv_description_product_detail.text = electronic.description
            electronic.image.let { listImageDetail.add(it) }
            loadImageDetail()
        }
    }

    private fun getDataFastFoodFromHome(){
        val food = intent?.extras?.getSerializable(Constant.PRODUCT_FAST_FOOD) as? FastFood?
        if (food != null){
            btn_buy_now.setOnClickListener {
                dialogConfirm.show(supportFragmentManager, "")
                dialogConfirm.isCancelable = false
            }
            tv_name_product_detail.text = food.name
            tv_price_product_detail.text = food.price.asMoney()
            tv_description_product_detail.text = food.description
            food.image.let { listImageDetail.add(it) }
            listImageDetail.add(food.image1)
            loadImageDetail()
        }
    }

}