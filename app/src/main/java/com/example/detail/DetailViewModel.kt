package com.example.detail

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.api.ApiService
import com.example.api.Response
import com.example.api.isSuccessResponse
import com.example.base.BaseViewModel
import com.example.constant.Constant
import com.example.constant.RealPathUtil
import com.example.database.AppDatabase
import com.example.model.AICheckAge
import com.example.model.AICheckComment
import com.example.model.Comment
import com.example.model.Fashion
import com.google.android.gms.common.api.Api
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import rx.Scheduler
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File
import java.util.function.LongFunction

class DetailViewModel : BaseViewModel() {
    val responseCheckAge = MutableLiveData<String>()
    val listComment = MutableLiveData<List<Comment>>()
    var toxicResponse = MutableLiveData<String>()
    private val mListComment = ArrayList<Comment>()
    private val reference = FirebaseDatabase.getInstance().reference

    fun uploadFileToServer(uri: Uri, context: Context) {
        if (uri == null) {
            return
        }
        val realPath = RealPathUtil.getPathFromURI(context, uri)
        val file = File(realPath)
        val requestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
        val pic = MultipartBody.Part.createFormData(Constant.PIC_PARAM, file.name, requestBody)
        ApiService.apiRequest().postImage(pic)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { showLoading() }
            .doOnError { hideLoading() }
            .doOnNext { hideLoading() }
            .subscribe(object : Subscriber<AICheckAge>() {
                override fun onCompleted() {

                }

                override fun onError(e: Throwable?) {
                    Log.e("e", e?.message.toString())
                }

                override fun onNext(response: AICheckAge?) {
                    responseCheckAge.value = response?.age!!
                }

            })
    }


    fun uploadFileToServerByCamera(file: File, context: Context) {
        if (file == null) {
            return
        }
        val requestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
        val pic = MultipartBody.Part.createFormData(Constant.PIC_PARAM, file.name, requestBody)
        ApiService.apiRequest().postImage(pic)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { showLoading() }
            .doOnError { hideLoading() }
            .doOnNext { hideLoading() }
            .subscribe(object : Subscriber<AICheckAge>() {
                override fun onCompleted() {

                }

                override fun onError(e: Throwable?) {
                    Log.e("e", e?.message.toString())
                }

                override fun onNext(response: AICheckAge?) {
                    responseCheckAge.value = response?.age!!
                }

            })
    }

}