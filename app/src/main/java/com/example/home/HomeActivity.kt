package com.example.home

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.adapter.HomeAdapter
import com.example.base.BaseActivity
import com.example.base.DialogBack
import com.example.base.initViewModel
import com.example.fragment.*
import com.example.scientific_research.R
import com.example.search.SearchProductFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity<HomeViewModel>(), DialogBack.OnClickConfirm{
    private val homeFragment = HomeFragment()
    private val cartFragment = CartFragment()
    private val notifyFragment = NotifyFragment()
    private val profileFragment = ProfileFragment()
    private val searchFragment = SearchProductFragment.newInstance()
    private val dialogBack = DialogBack()

    private var activeFragment : Fragment = homeFragment

    private val manager = supportFragmentManager

    private val navigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener{item ->
            return@OnNavigationItemSelectedListener when(item.itemId){
                R.id.ic_home -> {
                    manager.beginTransaction().hide(activeFragment).show(homeFragment).commit()
                    activeFragment = homeFragment
                    true
                }

                R.id.ic_cart -> {
                    manager.beginTransaction().hide(activeFragment).show(cartFragment).commit()
                    activeFragment = cartFragment
                    true
                }

                R.id.ic_noti -> {
                    manager.beginTransaction().hide(activeFragment).show(notifyFragment).commit()
                    activeFragment = notifyFragment
                    true
                }

                R.id.ic_account -> {
                    manager.beginTransaction().hide(activeFragment).show(profileFragment).commit()
                    activeFragment = profileFragment
                    true
                }

                else -> false
            }
        }

    override fun onCreateViewModel(): HomeViewModel= initViewModel()

    override fun layoutId(): Int = R.layout.activity_home

    override fun initView() {
        bottom_nav.background = null
        initBottomNavigation()
            btn_search.setOnClickListener {
                if (manager.findFragmentByTag("5") == null){
                    manager.beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_top, R.anim.slide_in_top, R.anim.slide_out_bottom)
                        .add(R.id.vp_main, searchFragment, "5")
                        .hide(activeFragment)
                        .commit()
                    coordinator_main.visibility = View.GONE
                }else{
                    manager.beginTransaction().remove(manager.findFragmentByTag("5")!!).commit()
                }

            }
    }

    override fun observerData() {

    }



    private fun initBottomNavigation(){
        bottom_nav.setOnNavigationItemSelectedListener(navigationItemSelectedListener)
        manager.beginTransaction().add(R.id.vp_main, homeFragment, "1").commit()

        manager.beginTransaction().add(R.id.vp_main, cartFragment, "2")
            .hide(cartFragment).commit()

        manager.beginTransaction().add(R.id.vp_main, notifyFragment, "3")
            .hide(notifyFragment).commit()

        manager.beginTransaction().add(R.id.vp_main, profileFragment, "4")
            .hide(profileFragment).commit()
    }


    override fun onClickYes() {
        finish()
    }

    override fun onClickNo() {
        dialogBack.dismiss()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            manager.beginTransaction()
                .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_bottom,R.anim.slide_in_bottom, R.anim.slide_out_top)
                .show(activeFragment)
                .hide(searchFragment).commit()
            coordinator_main.visibility = View.VISIBLE
        }
        return true
    }




}