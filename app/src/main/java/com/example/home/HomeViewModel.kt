package com.example.home

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.base.BaseViewModel
import com.example.base.readJsonFromAssets
import com.example.constant.Constant
import com.example.database.AppDatabase
import com.example.model.*
import com.google.firebase.database.*
import org.json.JSONObject

class HomeViewModel : BaseViewModel() {

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var databaseReference: DatabaseReference
    var listFashion = MutableLiveData<List<Fashion>>()
    var listHouseware = MutableLiveData<List<Houseware>>()
    var listElectronicDevice = MutableLiveData<List<ElectronicDevice>>()
    var listFastFood = MutableLiveData<List<FastFood>>()
    var listCigarette = MutableLiveData<List<Cigarette>>()

    private var mListFashion = ArrayList<Fashion>()
    private var mListHouseware = ArrayList<Houseware>()

    private var mListElectronic = ArrayList<ElectronicDevice>()

    private var mListFastFood = ArrayList<FastFood>()
    private var mListCigarette = ArrayList<Cigarette>()

    fun getJsonDataFromAssets(context: Context) {
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.reference
        val jsonFileString = readJsonFromAssets(context, "fashion.json")
        val jsonObject = JSONObject(jsonFileString)
        val jsonArray = jsonObject.getJSONArray("Fashion")
        for (i in 0 until jsonArray.length()) {
            val jsonObjectIndex = jsonArray.getJSONObject(i)

            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val fashion =
                Fashion(id, name, price, image, isCheck, number, description, numberProduct, null)
            AppDatabase.getDatabaseInstance(context).FashionDAO().insertFashion(fashion)
            val product = Product(id, name, price, image, null, isCheck, number, description, numberProduct)

        }
        for (i in 0 until 3){
            val jsonObjectIndex = jsonArray.getJSONObject(i)

            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val fashion =
                Fashion(id, name, price, image, isCheck, number, description, numberProduct, null)
            mListFashion.add(fashion)
        }
        listFashion.value = mListFashion

        val jsonArrayHouseWare = jsonObject.getJSONArray("Houseware")
        for (i in 0 until jsonArray.length()) {
            val jsonObjectIndex = jsonArrayHouseWare.getJSONObject(i)

            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val image1 = jsonObjectIndex.getString("image1")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val houseware = Houseware(
                id,
                name,
                price,
                image,
                image1,
                isCheck,
                number,
                description,
                numberProduct,
                null
            )
           AppDatabase.getDatabaseInstance(context).houseWareDAO().insertHouseWare(houseware)
        }

        for (i in 0 until 3){
            val jsonObjectIndex = jsonArrayHouseWare.getJSONObject(i)

            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val image1 = jsonObjectIndex.getString("image")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val houseware =
                Houseware(id, name, price, image, image1, isCheck, number, description, numberProduct, null)
            mListHouseware.add(houseware)
        }
        listHouseware.value = mListHouseware


        val arrElectronic = jsonObject.getJSONArray("ElectronicDevice")
        for (i in 0 until jsonArray.length()) {
            val jsonObjectIndex = arrElectronic.getJSONObject(i)

            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val electronicDevice = ElectronicDevice(
                id,
                name,
                price,
                image,
                isCheck,
                number,
                description,
                numberProduct
            )
            AppDatabase.getDatabaseInstance(context).electronicDAO().insertElectronic(electronicDevice)
        }
        for (i in 0 until 3){
            val jsonObjectIndex = arrElectronic.getJSONObject(i)

            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val electronic =
                ElectronicDevice(id, name, price, image, isCheck, number, description, numberProduct)
            mListElectronic.add(electronic)
        }
        listElectronicDevice.value = mListElectronic


        val arrFastFood = jsonObject.getJSONArray("Fast_food")
        for (i in 0 until arrFastFood.length()) {
            val jsonObjectIndex = arrFastFood.getJSONObject(i)
            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val image1 = jsonObjectIndex.getString("image1")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val fastFood = FastFood(
                id,
                name,
                price,
                image,
                image1,
                isCheck,
                number,
                description,
                numberProduct
            )
            AppDatabase.getDatabaseInstance(context).fastFoodDAO().insertFastFood(fastFood)
        }

        for (i in 0 until 3){
            val jsonObjectIndex = arrFastFood.getJSONObject(i)

            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val image1 = jsonObjectIndex.getString("image")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val fastFood =
                FastFood(id, name, price, image, image1, isCheck, number, description, numberProduct)
            mListFastFood.add(fastFood)
        }
        listFastFood.value = mListFastFood


        val arrCigarette = jsonObject.getJSONArray("Cigarette")
        for (i in 0 until arrCigarette.length()) {
            val jsonObjectIndex = arrCigarette.getJSONObject(i)
            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val image1 = jsonObjectIndex.getString("image1")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val cigarette = Cigarette(
                id,
                name,
                price,
                image,
                image1,
                isCheck,
                number,
                description,
                numberProduct,
            )
            AppDatabase.getDatabaseInstance(context).cigaretteDAO().insertCigarette(cigarette)
        }
        for (i in 0 until 3){
            val jsonObjectIndex = arrCigarette.getJSONObject(i)

            val id = jsonObjectIndex.getInt("id")
            val name = jsonObjectIndex.getString("name")
            val price = jsonObjectIndex.getLong("price")
            val image = jsonObjectIndex.getString("image")
            val image1 = jsonObjectIndex.getString("image")
            val isCheck = jsonObjectIndex.getBoolean("isCheck")
            val number = jsonObjectIndex.getInt("number")
            val description = jsonObjectIndex.getString("description")
            val numberProduct = jsonObjectIndex.getLong("numberProduct")

            val cigarette =
                Cigarette(id, name, price, image, image1, isCheck, number, description, numberProduct)
            mListCigarette.add(cigarette)
        }
        listCigarette.value = mListCigarette
    }

}