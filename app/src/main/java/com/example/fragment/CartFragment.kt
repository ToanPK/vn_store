package com.example.fragment

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.adapter.CartAdapter
import com.example.base.App
import com.example.base.BaseFragment
import com.example.base.initViewModel
import com.example.database.AppDatabase
import com.example.model.Cart
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.fragment_cart.*

class CartFragment : BaseFragment<CartViewModel>(), CartAdapter.OnClickProductCart {

    private val cartAdapter = CartAdapter()

    override fun layoutId(): Int = R.layout.fragment_cart

    override fun initView() {
        cartAdapter.listener = this
        if (AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().getListCart().isEmpty()){
            tv_product_empty.visibility = View.VISIBLE
            rcv_cart.visibility = View.GONE
        }else{
            tv_product_empty.visibility = View.GONE
            rcv_cart.visibility = View.VISIBLE
        }
        rcv_cart.layoutManager = LinearLayoutManager(context)

    }

    override fun observerData() {
        cartAdapter.submitList(AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().getListCart())
        rcv_cart.adapter = cartAdapter
        refreshData()
    }

    private fun refreshData(){
        refresh.setOnRefreshListener {
            cartAdapter.submitList(AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().getListCart())
            rcv_cart.adapter = cartAdapter
            refresh.isRefreshing = true
            refresh.isRefreshing = false
            if (AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().getListCart().isEmpty()){
                tv_product_empty.visibility = View.VISIBLE
                rcv_cart.visibility = View.GONE
            }else{
                tv_product_empty.visibility = View.GONE
                rcv_cart.visibility = View.VISIBLE
            }
        }

    }

    override fun onCreateViewModel(): CartViewModel = initViewModel()
    override fun onClick(cart: Cart) {
        AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().delete(cart)
        cartAdapter.submitList(AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().getListCart())
        rcv_cart.adapter = cartAdapter
        cartAdapter.notifyDataSetChanged()
    }
}