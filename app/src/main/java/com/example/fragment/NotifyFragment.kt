package com.example.fragment

import com.example.base.BaseFragment
import com.example.base.initViewModel
import com.example.scientific_research.R

class NotifyFragment : BaseFragment<NotifyViewModel>() {
    override fun layoutId(): Int = R.layout.fragment_notify

    override fun initView() {

    }

    override fun observerData() {

    }

    override fun onCreateViewModel(): NotifyViewModel = initViewModel()
}