package com.example.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.PagerAdapter
import com.example.adapter.*
import com.example.base.BaseFragment
import com.example.base.initViewModel
import com.example.base.showToast
import com.example.constant.Constant
import com.example.database.AppDatabase
import com.example.detail.DetailActivity
import com.example.home.HomeViewModel
import com.example.list.ListProductActivity
import com.example.model.*
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.fragment_home.*
import java.lang.Exception
import java.sql.Time
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : BaseFragment<HomeViewModel>(), FashionAdapter.OnClickItemFashion, HousewareAdapter.OnClickItemHouseWare,
CigaretteAdapter.OnClickCigarette,
FastFoodAdapter.OnClickItemFood,
ElectronicAdapter.OnClickElectronic{

    private lateinit var fashionAdapter : FashionAdapter
    private lateinit var housewareAdapter : HousewareAdapter
    private lateinit var electronicAdapter : ElectronicAdapter
    private lateinit var cigaretteAdapter : CigaretteAdapter

    private lateinit var fastFoodAdapter : FastFoodAdapter
    private val mListFashion = ArrayList<Fashion>()
    private var currentPager = 0

    private val listImage : List<Int> = listOf(
        R.drawable.img_banner_4,
        R.drawable.img_banner_2,
        R.drawable.img_banner_3,
        R.drawable.img_banner_5
    )

    private lateinit var pagerAdapter : HomeAdapter
    override fun layoutId(): Int = R.layout.fragment_home

    override fun initView() {
        rcv_fashion.layoutManager = LinearLayoutManager(context)
        rcv_house_ware.layoutManager = LinearLayoutManager(context)
        rcv_electronic_device.layoutManager = LinearLayoutManager(context)
        rcv_fast_food.layoutManager = LinearLayoutManager(context)
        rcv_cigarette.layoutManager = LinearLayoutManager(context)
        tv_see_more_fashion.setOnClickListener {
            startActivity(Intent(context, ListProductActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putInt(Constant.KEY_FASHION, 1)
                putExtras(bundle)
            })
        }

        tv_see_more_houseware.setOnClickListener {
            startActivity(Intent(context, ListProductActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putInt(Constant.KEY_HOUSE_WARE, 2)
                putExtras(bundle)
            })
        }

        tv_see_more_electronic.setOnClickListener {
            startActivity(Intent(context, ListProductActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putInt(Constant.KEY_ELECTRONIC, 3)
                putExtras(bundle)
            })
        }

        tv_see_more_food.setOnClickListener {
            startActivity(Intent(context, ListProductActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putInt(Constant.KEY_FAST_FOOD, 4)
                putExtras(bundle)
            })
        }

        tv_see_more_cigarette.setOnClickListener {
            startActivity(Intent(context, ListProductActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putInt(Constant.KEY_CIGARETTE, 5)
                putExtras(bundle)
            })
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun observerData() {
        pagerAdapter = context?.let { HomeAdapter(listImage, it) }!!
        vp_home.adapter = pagerAdapter
        indicator.setViewPager(vp_home)

        val update = Runnable {
            if (currentPager == listImage.size){
                currentPager = 0
            }
            try {
                vp_home.setCurrentItem(currentPager++, true)
            }catch (ex : Exception){
                ex.printStackTrace()
            }
        }

        val timer = Timer()
        val handler = Handler()
        timer.schedule(object : TimerTask(){
            override fun run() {
                handler.post(update)
            }
        }, Constant.DELAY_MS, Constant.PERIOD_MS)
        viewModel.getJsonDataFromAssets(requireContext())
        viewModel.listFashion.observe(this, androidx.lifecycle.Observer { listFashion ->
            fashionAdapter = FashionAdapter(listFashion)
            fashionAdapter.notifyDataSetChanged()
            rcv_fashion.adapter = fashionAdapter
            fashionAdapter.listener = this
        })

        viewModel.listHouseware.observe(this){ listHouseware ->
            housewareAdapter = HousewareAdapter(listHouseware)
            rcv_house_ware.adapter = housewareAdapter
            housewareAdapter.listener = this
        }

        viewModel.listElectronicDevice.observe(this){ listElectronic ->
            electronicAdapter = ElectronicAdapter(listElectronic)
            rcv_electronic_device.adapter = electronicAdapter
            electronicAdapter.listener = this
        }

        viewModel.listFastFood.observe(this){listFastFood ->
            fastFoodAdapter = FastFoodAdapter(listFastFood)
            rcv_fast_food.adapter = fastFoodAdapter
            fastFoodAdapter.listener = this
        }

        viewModel.listCigarette.observe(this){listCigarette ->
            cigaretteAdapter = CigaretteAdapter(listCigarette)
            rcv_cigarette.adapter = cigaretteAdapter
            cigaretteAdapter.listener = this
        }

    }


    override fun onCreateViewModel(): HomeViewModel = initViewModel()
    override fun onClickFashionDetail(position: Int, fashion: Fashion) {
        val bundle = Bundle()
        bundle.putSerializable(Constant.FASHION,fashion)
        val intent = Intent(context, DetailActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
    }

    override fun onClickFashionAddCart(position: Int, seeMore : ImageView, fashion: Fashion) {
        val popupMenu = PopupMenu(context, seeMore)
        popupMenu.inflate(R.menu.item_pop_up)

        popupMenu.setOnMenuItemClickListener { menuItem ->
            if (menuItem.itemId == R.id.add_product){
                mListFashion.add(fashion)
                Log.d("listFashion", mListFashion.size.toString())
                AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().insertCart(
                    Cart(null, fashion, null, null, null, null)
                )

                Log.d("data", AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().getListCart().size.toString())
                context?.let { showToast("Đã thêm thành công sản phẩm vào giỏ!", it) }
            }
            true
        }

        popupMenu.show()
    }



    override fun onClickHouseWare(position: Int, seeMore: ImageView, houseware: Houseware) {
        val popupMenu = PopupMenu(context, seeMore)
        popupMenu.inflate(R.menu.item_pop_up)

        popupMenu.setOnMenuItemClickListener { menuItem ->
            if (menuItem.itemId == R.id.add_product){
                AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().insertCart(
                    Cart(null, null, null, houseware, null, null)
                )

                Log.d("data", AppDatabase.getDatabaseInstance(requireContext().applicationContext).cartDAO().getListCart().size.toString())
                context?.let { showToast("Đã thêm thành công sản phẩm vào giỏ!", it) }
            }
            true
        }

        popupMenu.show()
    }

    override fun onClick(houseware: Houseware) {
        val bundle = Bundle()
        bundle.putSerializable(Constant.PRODUCT_HOUSE_WARE,houseware)
        val intent = Intent(context, DetailActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
    }

    override fun onClick(cigarette: Cigarette) {
        val bundle = Bundle()
        bundle.putSerializable(Constant.PRODUCT_CIGARETTE, cigarette)
        val intent = Intent(context, DetailActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
    }

    override fun onClick(food: FastFood) {
        val bundle = Bundle()
        bundle.putSerializable(Constant.PRODUCT_FAST_FOOD, food)
        val intent = Intent(context, DetailActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
    }

    override fun onClick(electronicDevice: ElectronicDevice) {
        val bundle = Bundle()
        bundle.putSerializable(Constant.PRODUCT_ELECTRONIC, electronicDevice)
        val intent = Intent(context, DetailActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
    }

}
