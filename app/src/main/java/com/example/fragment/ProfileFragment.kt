package com.example.fragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.example.base.BaseFragment
import com.example.base.initViewModel
import com.example.base.showCustomToast
import com.example.constant.Constant
import com.example.model.User
import com.example.scientific_research.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.fragment_profile.*
import java.net.URI


class ProfileFragment : BaseFragment<ProfileViewModel>() {
    private lateinit var imageUrl : Uri
    private val userId = FirebaseAuth.getInstance().currentUser
    private val databaseFirebase = FirebaseDatabase.getInstance()
    private lateinit var storeReference : StorageReference
    private lateinit var reference : DatabaseReference
    override fun layoutId(): Int = R.layout.fragment_profile

    override fun initView() {
        img_profile.setOnClickListener {
            requestPermissionAndPickImage()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        storeReference = FirebaseStorage.getInstance().getReference("uploads")
    }

    override fun observerData() {
        getInformationUserCurrent()
    }
    private fun getInformationUserCurrent(){
        reference = databaseFirebase.getReference("Users").child(userId!!.uid)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val user : User? = snapshot.getValue(User::class.java)
                if (user?.imageUrl == "default"){
                    img_profile.setImageResource(R.drawable.ic_avt)
                }else{
                    Glide.with(this@ProfileFragment).load(user?.imageUrl).into(img_profile)
                }
                tv_user_name_profile.text = user?.userName
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("error_profile", error.message)
            }

        })
    }

    private fun openImage(){
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, Constant.IMAGE_REQUEST)
    }

    private fun getFileExtension(uri : Uri) : String?{
        val contentResolve = context?.contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getExtensionFromMimeType(contentResolve?.getType(uri))
    }

    private fun uploadImage(){
         if(imageUrl != null){
             val storageRef = storeReference.child(System.currentTimeMillis().toString() + "." + getFileExtension(imageUrl))
             val uploadTask = storageRef?.putFile(imageUrl)
             uploadTask?.continueWith { task ->
                 if(task.isSuccessful){
                     storageRef.downloadUrl
                 }
             }?.addOnCompleteListener{ task ->
                     if(task.isSuccessful){
                             val downloadUri : Uri? = task.result as Uri
                             val url = downloadUri.toString()
                             reference = FirebaseDatabase.getInstance().getReference("Users").child(userId!!.uid)
                             val mapUpdate : HashMap<String, Any?> = hashMapOf()
                             mapUpdate["imageUrl"] = url
                             reference.updateChildren(mapUpdate)
                         }else{
                             context.let {
                                 Toast(context).showCustomToast("Đã có lỗi xảy ra xin vui lòng thử lại sau!", false,
                                     it as Activity
                                 )
                             }
                         }
                     }
        ?.addOnFailureListener { exception ->
                 this.activity?.let {
                     Toast(context).showCustomToast(exception.message.toString(), false,
                         it
                     )
                 }
             }
         }else{
             this.activity?.let {
                 Toast(context).showCustomToast("no image selected!", false,
                     it
                 )
             }
         }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.IMAGE_REQUEST && resultCode == Activity.RESULT_OK){
            if(data != null && data.data != null){
                imageUrl = data.data!!
                uploadImage()
            }
        }
    }

    private fun requestPermissionAndPickImage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            openImage()
            return
        }
        val result = context?.let {
            ContextCompat.checkSelfPermission(
                it,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        }
        if (result == PackageManager.PERMISSION_GRANTED) {
            openImage()
        } else {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), Constant.READ_EXTERNAL_REQUEST
            )
        }
    }
    override fun onCreateViewModel(): ProfileViewModel = initViewModel()
}