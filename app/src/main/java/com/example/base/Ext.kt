package com.example.base

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.Patterns
import android.view.Gravity
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.scientific_research.R
import java.io.IOException
import java.text.NumberFormat
import java.util.*


@JvmName("initViewModel")
inline fun <reified T : ViewModel> AppCompatActivity.initViewModel() =
    ViewModelProviders.of(this, ViewModelFactory.getInstance(this)).get(T::class.java)

inline fun <reified T : ViewModel> Fragment.initViewModel() =
    ViewModelProviders.of(this, ViewModelFactory.getInstance(this.requireContext()))
        .get(T::class.java)

fun AppCompatActivity.replaceFragment(
    frg: Fragment,
    @IdRes container: Int,
    isAddToBackStack: Boolean
) {
    val fragmentManager = this.supportFragmentManager
    val transaction = fragmentManager.beginTransaction()
    transaction.replace(container, frg)
    if (isAddToBackStack) {
        transaction.addToBackStack("Toan PK")
    }
    transaction.commit()
}

fun isValidateEmail(email: CharSequence): Boolean {
    return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches())
}

fun showToast(text: String, context: Context) {
    Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
}

fun readJsonFromAssets(context: Context, fileName: String): String? {
    val jsonString: String
    try {
        jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
    } catch (IOException: IOException) {
        IOException.printStackTrace()
        return null
    }
    return jsonString
}

fun Long.asMoney(): String {
    val numberFormat = NumberFormat.getIntegerInstance(Locale("vi"))
    val formatted = numberFormat.format(this)
    return "$formatted đ"
}

fun ImageView.loadImage(image: String?) {
    Glide.with(context).load(Uri.parse("file:///android_asset/fashion/$image")).into(this)
}

fun Toast.showCustomToast(message: String, check: Boolean, activity: Activity) {
    val layout = activity.layoutInflater.inflate(
        R.layout.custom_toast_app,
        activity.findViewById(R.id.toast_container)
    )
    val textToast = layout.findViewById<TextView>(R.id.tv_toast)
    textToast.text = message
    val imageToast = layout.findViewById<ImageView>(R.id.img_toast)
    if (check) {
        imageToast.setImageResource(R.drawable.icon_success)
    } else {
        imageToast.setImageResource(R.drawable.icon_error)
    }

    this.apply {
        setGravity(Gravity.BOTTOM, 0, 40)
        duration = Toast.LENGTH_LONG
        view = layout
        show()
    }
}
fun String.formatDate() : String {
    val calendar = Calendar.getInstance(Locale.ENGLISH).time
    return DateFormat.format("hh:mm (dd/MM/yyyy)", calendar).toString()
}