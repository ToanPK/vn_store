package com.example.base

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.detail.DetailViewModel
import com.example.fragment.CartViewModel
import com.example.fragment.NotifyViewModel
import com.example.fragment.ProfileViewModel
import com.example.home.HomeViewModel
import com.example.list.ListProductViewModel
import com.example.scientific_research.MainViewModel
import com.example.search.SearchProductViewModel
import com.example.signup.SignUpViewModel
import com.example.splash.SplashAppViewModel
import java.lang.IllegalStateException

class ViewModelFactory(context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass){
            when{
                isAssignableFrom(SplashAppViewModel::class.java) -> {
                    SplashAppViewModel()
                }
                isAssignableFrom(MainViewModel::class.java) -> {
                    MainViewModel()
                }
                isAssignableFrom(SignUpViewModel::class.java) -> {
                    SignUpViewModel()
                }
                isAssignableFrom(HomeViewModel::class.java) -> {
                    HomeViewModel()
                }

                isAssignableFrom(CartViewModel::class.java) -> {
                    CartViewModel()
                }

                isAssignableFrom(NotifyViewModel::class.java) -> {
                    NotifyViewModel()
                }

                isAssignableFrom(ProfileViewModel::class.java) -> {
                    ProfileViewModel()
                }

                isAssignableFrom(DetailViewModel::class.java) -> {
                    DetailViewModel()
                }
                isAssignableFrom(SearchProductViewModel::class.java) -> {
                    SearchProductViewModel()
                }
                isAssignableFrom(ListProductViewModel::class.java) -> {
                    ListProductViewModel()
                }
                else -> throw IllegalStateException("unknow viewModel : $modelClass")
            } as T
    }

    companion object{
        fun getInstance(context: Context) : ViewModelFactory = ViewModelFactory(context)
    }
}