package com.example.base

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.scientific_research.R
import kotlinx.android.synthetic.main.item_dialog.*

class DialogBack : DialogFragment() {
    var listener : OnClickConfirm?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog!!.window!!.setBackgroundDrawableResource(R.color.transparent)
        return inflater.inflate(R.layout.item_dialog, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_yes.setOnClickListener {
            listener?.onClickYes()
        }

        btn_no.setOnClickListener {
            listener?.onClickNo()
        }


    }

    interface OnClickConfirm{
        fun onClickYes()
        fun onClickNo()
    }

}