package com.example.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    val isLoading = MutableLiveData<Boolean>()

    fun showLoading(){
        isLoading.postValue(true)
    }

    fun hideLoading(){
        isLoading.postValue(false)
    }
}