package com.example.base

import android.os.Bundle
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import android.os.Build
import android.view.KeyEvent
import android.view.Window


abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {
    private lateinit var internalViewModel : VM

    private val dialogBack = DialogBack()

    private lateinit var circleDialog: CircleDialog

    abstract fun onCreateViewModel() : VM
    val viewModel get() = internalViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        circleDialog = CircleDialog(this)
        internalViewModel = onCreateViewModel()
        initView()
        observerData()
        // In Activity's onCreate() for instance
        // In Activity's onCreate() for instance
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w: Window = window
            w.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
            )
        }

    }
    @LayoutRes
    abstract fun layoutId() : Int
    abstract fun initView()
    abstract fun observerData()


    fun showOrHideProgressDialog(isLoading : Boolean){
        when(isLoading){
            true -> if(!(this).isFinishing){
                circleDialog.show()
            }else -> circleDialog.dismiss()
        }
    }

    override fun onPause() {
        super.onPause()
        showOrHideProgressDialog(false)
    }





}