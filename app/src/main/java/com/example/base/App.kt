package com.example.base

import android.app.Application
import com.example.database.AppDatabase

class App : Application(){
    private lateinit var dbInstance : AppDatabase

    companion object{
        private lateinit var instance : App

        fun getInstanceApp() : App = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        dbInstance = AppDatabase.getDatabaseInstance(applicationContext)!!
    }
}