package com.example.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment<VM : BaseViewModel> : Fragment() {

    private lateinit var internalViewModel : VM
    val viewModel : VM get() = internalViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        internalViewModel = onCreateViewModel()
        initView()
        observerData()
    }

    @LayoutRes
    abstract fun layoutId() : Int
    abstract fun initView()
    abstract fun observerData()
    abstract fun onCreateViewModel() : VM

}