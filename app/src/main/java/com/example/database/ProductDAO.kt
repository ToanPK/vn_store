package com.example.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.model.Product

@Dao
interface ProductDAO : BaseDAO<Product>{
    @Query("SELECT * FROM product")
    fun getListProduct() : List<Product>

}