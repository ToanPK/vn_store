package com.example.database

import android.content.Context
import androidx.room.*
import com.example.model.*

@Database(
    entities = [Cart::class, Cigarette::class,
        ElectronicDevice::class, Fashion::class,
        FastFood::class, Houseware::class,
        Comment::class, Product::class],
    version = 1
)
@TypeConverters(ConvertData::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cartDAO(): CartDAO
    abstract fun FashionDAO(): FashionDAO
    abstract fun houseWareDAO() : HousewareDAO
    abstract fun electronicDAO() : ElectronicDAO
    abstract fun fastFoodDAO() : FastFoodDAO
    abstract fun cigaretteDAO() : CigaretteDAO
    abstract fun commentDAO(): CommentDAO
    abstract fun productDAO() : ProductDAO

    companion object {
        private var dbInstance: AppDatabase? = null

        fun getDatabaseInstance(context: Context): AppDatabase {
            if (dbInstance == null) {
                dbInstance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "vn_store"
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return dbInstance!!
        }
    }

}