package com.example.database

import androidx.room.*
import com.example.model.Cart
import com.example.model.Fashion


@Dao
interface CartDAO : BaseDAO<Cart> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCart(cart: Cart)

    @Query("SELECT * FROM cart")
    fun getListCart() : List<Cart>

    @Query("SELECT * FROM cart WHERE fashion =:fashion")
    fun getListFashionFromCart(fashion : Fashion) : List<Cart>

}