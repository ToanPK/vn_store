package com.example.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.model.ElectronicDevice
import com.example.model.Houseware


@Dao
interface ElectronicDAO : BaseDAO<ElectronicDevice> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertElectronic(electronic : ElectronicDevice)

    @Query("SELECT * FROM ElectronicDevice")
    fun getListElectronic() : List<ElectronicDevice>
}