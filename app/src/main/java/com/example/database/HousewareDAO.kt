package com.example.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.model.Fashion
import com.example.model.Houseware


@Dao
interface HousewareDAO : BaseDAO<Houseware>{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHouseWare(houseware: Houseware)

    @Query("SELECT * FROM houseware")
    fun getListHouseWare() : List<Houseware>
}