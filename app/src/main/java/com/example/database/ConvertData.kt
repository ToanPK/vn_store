package com.example.database
import androidx.room.TypeConverter
import com.example.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ConvertData  {

    @TypeConverter
    fun fromFashionList(value : Fashion?) : String{
        val gson = Gson()
        val type = object : TypeToken<Fashion?>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toFashionList(value : String) : Fashion?{
        val gson = Gson()
        val type = object : TypeToken<Fashion?>(){}.type
        return gson.fromJson(value, type)
    }


    @TypeConverter
    fun fromFastFoodList(value : FastFood?) : String{
        val gson = Gson()
        val type = object : TypeToken<List<FastFood>?>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toFastFoodList(value : String) : FastFood?{
        val gson = Gson()
        val type = object : TypeToken<FastFood?>(){}.type
        return gson.fromJson(value, type)
    }


    @TypeConverter
    fun fromElectronicList(value : ElectronicDevice?) : String{
        val gson = Gson()
        val type = object : TypeToken<ElectronicDevice?>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toElectronicList(value : String) : ElectronicDevice?{
        val gson = Gson()
        val type = object : TypeToken<ElectronicDevice?>(){}.type
        return gson.fromJson(value, type)
    }
    @TypeConverter
    fun fromHouseWareList(value : Houseware?) : String{
        val gson = Gson()
        val type = object : TypeToken<Houseware?>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toHouseWareList(value : String) : Houseware?{
        val gson = Gson()
        val type = object : TypeToken<Houseware?>(){}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromCigaretteList(value : Cigarette?) : String{
        val gson = Gson()
        val type = object : TypeToken<Cigarette?>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toCigaretteList(value : String) : Cigarette?{
        val gson = Gson()
        val type = object : TypeToken<Cigarette>(){}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromCommentList(value : List<Comment>?) : String{
        val gson = Gson()
        val type = object : TypeToken<List<Comment>?>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toCommentList(value : String) : List<Comment>?{
        val gson = Gson()
        val type = object : TypeToken<List<Comment>?>(){}.type
        return gson.fromJson(value, type)
    }
}