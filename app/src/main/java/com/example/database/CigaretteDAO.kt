package com.example.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.model.Cigarette
import com.example.model.Houseware


@Dao
interface CigaretteDAO : BaseDAO<Cigarette> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCigarette(cigarette: Cigarette)

    @Query("SELECT * FROM Cigarette")
    fun getListCigarette() : List<Cigarette>
}