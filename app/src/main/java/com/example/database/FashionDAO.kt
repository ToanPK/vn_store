package com.example.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.model.Fashion


@Dao
interface FashionDAO : BaseDAO<Fashion>{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFashion(fashion: Fashion)

    @Query("SELECT * FROM fashion")
    fun getListFashion() : List<Fashion>
}