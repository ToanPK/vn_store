package com.example.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.model.FastFood
import com.example.model.Houseware


@Dao
interface FastFoodDAO : BaseDAO<FastFood>{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFastFood(fastFood : FastFood)

    @Query("SELECT * FROM fastfood")
    fun getListFastFood() : List<FastFood>

}